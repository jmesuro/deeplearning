import numpy
import theano
import theano.tensor as T
from PIL import Image
import glob
rng = numpy.random

n_airplanes = 800
n_motorbike = 798
cp = 28
N = n_airplanes + n_motorbike             # training sample size
feats = cp*cp*3                        # number of input variables
PATHMOT = '/home/jmesuro/Downloads/Caltech101/Motorbikes/*.jpg'
PATHAIR = '/home/jmesuro/Downloads/Caltech101/airplanes/*.jpg'


def im_homo(im):
	imaux = im.resize((cp,cp)).convert('RGB')
	d = list(imaux.getdata())
	l = [i[0] for i in d] + [i[1] for i in d] + [i[2] for i in d] 
	return l


def load_dataset():
	np = numpy
	i = 0
	B = np.zeros(dtype = np.uint8, shape = (N,feats))
	
	for filename in glob.glob(PATHMOT):  
		B[i,:] = im_homo(Image.open(filename))
		i = 1+i
	
	for filename in glob.glob(PATHAIR):  
		B[i,:] = im_homo(Image.open(filename))
		i = 1+i

	return (numpy.array(B),numpy.array([1]*n_airplanes + [0]*n_motorbike))

D = load_dataset()

x = T.dmatrix("x")
y = T.dvector("y")

h = 100 #size of hidden layer
W = theano.shared(rng.randn(feats,h),name ='W')
b = theano.shared(numpy.zeros((h)),name= 'b',)
 
W2 = theano.shared(rng.randn(h),name='W2')
b2 = theano.shared(0.,name= 'b2')

hidden_layer = T.nnet.relu(T.dot(x,W)+b)
 
s = - T.dot(hidden_layer,W2) - b2

p_1 = 1 / (1 + T.exp(s))   # Probability that target = 1
prediction = p_1 > 0.5                    # The prediction thresholded
xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1) # Cross-entropy loss function
cost = xent.mean() + 0.01*(W**2).sum() + 0.01*(W2**2).sum()
gW2, gb2,gW,gb = T.grad(cost, [W2, b2,W,b])             


lr = 0.1
# Compile
train = theano.function(
          inputs=[x,y],
          outputs=[prediction, xent],
          updates=((W2, W2 - lr * gW2), (b2, b2 - lr * gb2),(b,b-lr*gb),(W,W-lr*gW) ))
predict = theano.function(inputs=[x], outputs=prediction)



training_steps = 55
# Train
def grad_train():
	for i in range(training_steps):
    		print "training"
		pred, err = train(D[0], D[1])
	return pred, err

def diff(l1,l2):
	return sum(map(lambda (x,y):  x != y, zip(l1,l2) ))

# Train minibatch
#sl = range(0,N)
#rng.shuffle(sl)
#bsize = 94
#nbatch = N/bsize - 1
#for i in range(training_steps):
#	for b in range(0,nbatch*bsize,bsize):
#		pred, err  = train(D[0][sl[b:b+bsize]],D[1][sl[b:b+bsize]])


grad_train()
#print("Final model:")
#print(w.get_value())
#print(b.get_value())
print("target values for D:")
print(D[1])
print("prediction on D:")
print(predict(D[0]))


print "diferencia " + str( diff(list(D[1]), list(predict(D[0]))   ) )
print "error porcentual " + str( diff(list(D[1]), list(predict(D[0]))   )*100 / D[1].size   )
