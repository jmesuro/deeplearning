import numpy as np
from matplotlib.mlab import PCA
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

NWORDS = 1000

def clean_string(string):
	string = string.replace(',',' ')
	string = string.replace('.',' ')
	string = string.replace(';',' ')
	string = string.replace(':',' ')
	string = string.replace('?',' ')
	string = string.replace('!',' ')
	string = string.replace('-',' ')
	string = string.replace('"',' ')
	string = string.replace('\'',' ')
	string = string.replace('-',' ')
	string = string.replace(')',' ')
	string = string.replace('(',' ')
	string = string.replace('[',' ')
	string = string.replace(']',' ')
	string = string.replace('{',' ')
	string = string.replace('}',' ')
	string = string.replace('_',' ')
	string = string.replace('=',' ')
	return string	

# se le pasa un string cleaned y devuelve un dict
# con cada palabra como clave y como valor la cantidad de 
# veces que aparece
def count_words(cs):
	l = cs.split()
	map1 = dict()
	for word in l:
		if (word in map1.keys()):
			map1[word] = map1[word] + 1
		else:
			map1[word] = 1
	
	return map1 

# codifica las oraciones de big_list en na matriz numpy
# según el vector vect
def frec_sentence(vect,big_list):
	sentences = np.zeros(shape = (len(big_list),NWORDS) )
	i = 0
	for sentence in big_list:
		cs = clean_string(sentence)
		words_map = count_words(cs)
		j = 0
		for word in vect:
			if (word in words_map):
				sentences[i,j] = words_map[word]
			j = j+1
		i = i+1

	return sentences



def principal(path):

	full_txt_str = open(path).read()
	full_txt_l = full_txt_str.split('.')
	clean_full_txt = clean_string(full_txt_str)
	
	word_map = count_words(clean_full_txt)
	vmax = sorted(word_map,key=word_map.__getitem__,reverse=True)[0:NWORDS ]
	vmin = sorted(word_map,key=word_map.__getitem__)[0:NWORDS ]	

	sentences1 = frec_sentence(vmax,full_txt_l)
	sentences2 = frec_sentence(vmin,full_txt_l)
	result1 = PCA(sentences1)
	result2 = PCA(sentences2)


	ploting2d(result1)
	
	ploting2d(result2)
	

	return sentences

def ploting3d(result):
	x = list(result.Y[:,0])
	y = list(result.Y[:,1])
	z = list(result.Y[:,2])

	plt.close('all') # close all latent plotting windows
	fig1 = plt.figure() # Make a plotting figure
	ax = Axes3D(fig1) # use the plotting figure to create a Axis3D object.
	pltData = [x,y,z] 
	ax.scatter(pltData[0], pltData[1],pltData[2], 'bo') # make a scatter plot of blue dots from the data
 
	xAxisLine = ((min(pltData[0]), max(pltData[0])), (0, 0), (0,0)) # 2 points make the x-axis line at the data extrema along x-axis 
	ax.plot(xAxisLine[0], xAxisLine[1], xAxisLine[2], 'r') # make a red line for the x-axis.
	yAxisLine = ((0, 0), (min(pltData[1]), max(pltData[1])), (0,0)) # 2 points make the y-axis line at the data extrema along y-axis
	ax.plot(yAxisLine[0], yAxisLine[1], yAxisLine[2], 'r') # make a red line for the y-axis.
	zAxisLine = ((0, 0), (0,0), (min(pltData[2]), max(pltData[2]))) # 2 points make the z-axis line at the data extrema along z-axis
	ax.plot(zAxisLine[0], zAxisLine[1], zAxisLine[2], 'r') # make a red line for the z-axis.
  	
# label the axes 
	ax.set_xlabel("x-axis label") 
	ax.set_ylabel("y-axis label")
	ax.set_zlabel("z-axis label")
	ax.set_title("PCA 3d")
	plt.show() # show the plot	

def ploting2d(result):
	fig,ax = plt.subplots()
	#PCA te devuelve los features ordenados en importancia descendente
	ax.scatter(list(result.Y[:,0]),list(result.Y[:,1]))
	ax.set_xlabel("x-axis " + f1 ) 
	ax.set_ylabel("y-axis " + f2 )
	plt.show()
				


	
