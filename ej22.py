import numpy
import theano
import theano.tensor as T
from PIL import Image
import glob


n_airplanes = 800
n_motorbike = 798
cp = 28
PATHMOT = '/home/jmesuro/Downloads/Caltech101/Motorbikes/*.jpg'
PATHAIR = '/home/jmesuro/Downloads/Caltech101/airplanes/*.jpg'


def im_homo(im):
	imaux = im.resize((cp,cp)).convert('RGB')
	d = list(imaux.getdata())
	l = [i[0] for i in d] + [i[1] for i in d] + [i[2] for i in d] 
	return l


def load_dataset():
	np = numpy
	i = 0
	B = np.zeros(dtype = np.uint8, shape = (n_airplanes+n_motorbike,cp*cp*3))
	
	for filename in glob.glob(PATHMOT):  
		B[i,:] = im_homo(Image.open(filename))
		i = 1+i
	
	for filename in glob.glob(PATHAIR):  
		B[i,:] = im_homo(Image.open(filename))
		i = 1+i

	return (numpy.array(B),numpy.array([1]*n_airplanes + [0]*n_motorbike))

D = load_dataset()
rng = numpy.random

N = n_airplanes + n_motorbike             # training sample size
feats = cp*cp*3                        # number of input variables

# generate a dataset: D = (input_values, target_class)

# Declare Theano symbolic variables
x = T.dmatrix("x")
y = T.dvector("y")

# initialize the weight vector w randomly
#
# this and the following bias variable b
# are shared so they keep their values
# between training iterations (updates)
w = theano.shared(rng.randn(feats), name="w")

# initialize the bias term
b = theano.shared(0., name="b")

#print("Initial model:")
#print(w.get_value())
#print(b.get_value())

# Construct Theano expression graph
p_1 = 1 / (1 + T.exp(-T.dot(x, w) - b))   # Probability that target = 1
prediction = p_1 > 0.5                    # The prediction thresholded
xent = -y * T.log(p_1) - (1-y) * T.log(1-p_1) # Cross-entropy loss function
cost = xent.mean() + 0.01 * (w ** 2).sum()# The cost to minimize
gw, gb = T.grad(cost, [w, b])             # Compute the gradient of the cost
                                          # w.r.t weight vector w and
                                          # bias term b
                                          # (we shall return to this in a
                                          # following section of this tutorial)

# Compile
train = theano.function(
          inputs=[x,y],
          outputs=[prediction, xent],
          updates=((w, w - 0.1 * gw), (b, b - 0.1 * gb)))
predict = theano.function(inputs=[x], outputs=prediction)


training_steps = 20

# Train
def grad_train():
	for i in range(training_steps):
    		pred, err = train(D[0], D[1])
	return pred, err

def diff(l1,l2):
	return sum(map(lambda (x,y):  x != y, zip(l1,l2) ))

# Train minibatch
bsize = 94
def mbatch_train(bsize):
	sl = range(0,N)
	rng.shuffle(sl)
	nbatch = N/bsize - 1
	for i in range(training_steps):
		for b in range(0,nbatch*bsize,bsize):
			pred, err  = train(D[0][sl[b:b+bsize]],D[1][sl[b:b+bsize]])

mbatch_train(bsize)
#print("Final model:")
#print(w.get_value())
#print(b.get_value())
print("target values for D:")
print(D[1])
print("prediction on D:")
print(predict(D[0]))


print "diferencia " + str( diff(list(D[1]), list(predict(D[0]))   ) )
print "error porcentual " + str( diff(list(D[1]), list(predict(D[0]))   )*100 / D[1].size   )
