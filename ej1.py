from PIL import Image
import glob
import numpy as np
import math
import random
from PIL import ImageOps	


PATH = '/home/joaquin/Downloads/deep-learning-course-master/test/'
UNPATH = PATH + 'words/*.png'
#PATH = '/home/joaquin/Downloads/Caltech101/'
#UNPATH = PATH + 'laptop/' + '*.jpg'
#PATH = '/home/joaquin/Downloads/deep-learning-course-master/test/words/' 

max_alto = 0
max_ancho = 0
n_images = 0
def init_global_vars():
	f1 = []
	f2 = []
	global max_alto
	global max_ancho
	global n_images
	for filename in glob.glob(UNPATH):
		im = Image.open(filename).convert('RGB')
		f1.append(im.size[0])
		f2.append(im.size[1])
		n_images = n_images + 1
	
	max_ancho = max(f1)
	max_alto = max(f2) 
#calculo el ancho y alto maximo de todas las fotos

def homogenization(img):
        ancho = img.size[0]
        alto = img.size[1]


	print alto
	print ancho
        off_alto = (max_alto - alto) / 2
        off_ancho = (max_ancho - ancho) / 2

	print off_alto
	print off_ancho
	print max_ancho
	A = np.zeros(shape=(max_alto,max_ancho,3),dtype = np.uint8)
 
	A[:,:,0] = np.asarray(img)[:,:,0].mean()
	A[:,:,1] = np.asarray(img)[:,:,1].mean()
	A[:,:,2] = np.asarray(img)[:,:,2].mean()
	A[off_alto:off_alto+alto , off_ancho:off_ancho+ancho,:] = np.asarray(img)
	return A	


def load_imgs():
	i = 0
	B = np.zeros(dtype = np.uint8, shape = (n_images,max_alto,max_ancho,3))
	for filename in glob.glob(UNPATH ):  
		im = Image.open(filename).convert('RGB')
		B[i,:,:,:] = homogenization(im)
		i = 1+i
	return B		



def random_window(im):
	x = math.floor(random.uniform(0,max_alto-17))
	y = math.floor(random.uniform(0,max_ancho-17))
	return im.crop((x,y,x+16,y+16))

def standarization(im):
	A = np.asarray(im, dtype=np.uint8)
	B = np.zeros(shape = A.shape, dtype = A.dtype)
	B[:,:,0] = (A[:,:,0] - A[:,:,0].mean()) / A[:,:,0].std()
	B[:,:,1] = (A[:,:,1] - A[:,:,1].mean()) / A[:,:,1].std()
	B[:,:,2] = (A[:,:,2] - A[:,:,2].mean()) / A[:,:,2].std()
	return Image.fromarray(B)

def standarization_lucas(im):
	A = np.asarray(im, dtype=np.uint8)
	B = np.zeros(shape = A.shape, dtype = A.dtype)
	B[:,:,0] = (A[:,:,0] -127) / 73.6
	B[:,:,1] = (A[:,:,1] -127) / 73.6
	B[:,:,2] = (A[:,:,2] -127) / 73.6
	return Image.fromarray(B)


def standarization2(im,I):
	A = np.asarray(im,dtype=np.uint8)
	B = np.zeros(shape = A.shape, dtype = A.dtype)
	B[:,:,0] = (A[:,:,0] - I[:,:,:,0].mean()) / I[:,:,:,0].std() 
        B[:,:,1] = (A[:,:,1] - I[:,:,:,1].mean()) / I[:,:,:,1].std()
        B[:,:,2] = (A[:,:,2] - I[:,:,:,2].mean()) / I[:,:,:,2].std()

	print "valores"
	print I[:,:,:,0].std()
	print I[:,:,:,1].std()
	print I[:,:,:,2].std()
	print I[:,:,:,0].mean()
	print I[:,:,:,1].mean()
	print I[:,:,:,2].mean()

        return Image.fromarray(B)	

def normalization(im):
	A = np.asarray(im, dtype=np.uint8)
	B = np.zeros(shape = A.shape, dtype = A.dtype)
	B[:,:,0] = (A[:,:,0] - A[:,:,0].min()) / A[:,:,0].max() - A[:,:,0].min()
	B[:,:,1] = (A[:,:,1] - A[:,:,1].min()) / A[:,:,1].max() - A[:,:,1].min()
	B[:,:,2] = (A[:,:,2] - A[:,:,2].min()) / A[:,:,2].max() - A[:,:,2].min()
	return Image.fromarray(B)



init_global_vars()
img = Image.open(PATH + 'words/un 03.png').convert('RGB')
imA = homogenization(img)
im = Image.fromarray(imA)
im.show()
A = load_imgs()
ims = standarization(im)
ims.show()
